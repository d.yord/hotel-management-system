package FileHandlers;

import java.io.*;
import java.util.List;

public class FileHandler<T extends Serializable> {

    public List<T> loadList(String path){
        try(InputStream file = new FileInputStream(path);
            ObjectInputStream objs = new ObjectInputStream(file)){
            return (List<T>) objs.readObject();
        }catch (IOException | ClassNotFoundException ex){
//            TODO error handling
            System.out.println(ex.getMessage());
            throw new RuntimeException();
        }
    }

    public void saveList(String path, List<T> objs){
        try(FileOutputStream file = new FileOutputStream(path);
            ObjectOutputStream stream = new ObjectOutputStream(file)){
            stream.writeObject(objs);
        }catch (IOException ex){
//            TODO error handling
            System.out.println(ex.getMessage());
            throw new RuntimeException();
        }
    }
}
