import FileHandlers.FileHandler;
import Models.Booking;
import Models.Enums.UserRole;
import Models.Hotels.Hotel;
import Models.Users.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HotelManagementSystem {
    public static final String ENTER_USERNAME_AND_PASSWORD_MSG = "Please enter username and password," +
            " separated by enter";
    public static final String WRONG_AUTHENTICATION = "Wrong authentication";
    public static final String MAIN_MENU_MSG = """
            Enter number:
            1 for login
            2 for registration
            3 for exit""";
    public static final String INVALID_MAIN_MENU_SELECTION = "The valid selection is 1 or 2, or 3";
    private List<Hotel> hotels;
    private List<Booking> bookings;
    private List<User> users;
    private User user;

    public HotelManagementSystem() {
        this.hotels = new ArrayList<>();
        this.bookings = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    private void start(){
        this.hotels = new FileHandler<Hotel> ().loadList("");
        this.bookings = new FileHandler<Booking> ().loadList("");
        this.users = new FileHandler<User> ().loadList("");
        this.user = null;
    }

    private void saveFiles(){
        try {
            FileHandler<Hotel> hotelFileHandler = new FileHandler<Hotel>();
            FileHandler<Booking> bookingFileHandler = new FileHandler<Booking>();
            FileHandler<User> userFileHandler = new FileHandler<User>();
            hotelFileHandler.saveList("", this.hotels);
            bookingFileHandler.saveList("", this.bookings);
            userFileHandler.saveList("", this.users);
        }catch (RuntimeException ex){
            ex.printStackTrace();
        }
    }

    private  boolean authenticateUser(){
        Scanner scanner = new Scanner(System.in);
        System.out.println(ENTER_USERNAME_AND_PASSWORD_MSG);
        String username = scanner.nextLine();
        String password = scanner.nextLine();
        this.user = this.users.stream()
                .filter(user1 -> (user1.getUsername().equals(username) && user1.getPassword().equals(password)))
                .findAny()
                .orElse( null);
        if (this.user == null){
            System.out.println(WRONG_AUTHENTICATION);
            return false;
        }
        return true;
    }

    private void registerUser(){
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();
        String username = scanner.nextLine();
        users.add(new User(username, password, UserRole.GUEST, new ArrayList<Booking>()));
    }

//   returns false to exit the programme, otherwise loads the user and continues
    private boolean showMainMenu(){
        Scanner scanner = new Scanner(System.in);
        boolean isAuthenticated = false;
        while (true) {
            System.out.println(MAIN_MENU_MSG);
            String selection = scanner.nextLine();
            switch (selection) {
                case "1":
                    isAuthenticated = this.authenticateUser();
                    break;
                case "2":
                    this.registerUser();
                    break;
                case "3":
                    return false;
                default:
                    System.out.flush();
                    System.out.println(INVALID_MAIN_MENU_SELECTION);
            }
            if(isAuthenticated){
                break;
            }
        }
        return true;
    }

    private void showUserMenu(){
//      show available rooms
//        add booking
//        cancel booking
    }

    private void showAdminMenu(){
//        add admin
//        add hotel
    }
    public void run(){
        this.start();
        while (this.showMainMenu()){
            System.out.flush();
            switch (this.user.getRole()){
                case GUEST:
                    this.showUserMenu();
                    break;
                case ADMIN:
                    this.showAdminMenu();
                    break;
            }
        }
        this.saveFiles();
    }
}
