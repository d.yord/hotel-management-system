package Models;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Booking implements Serializable {
    private int id;
    private int roomId;
    private int hotelId;
    private int guestId;
    private Date startDate;
    private Date endDate;
    private double price;
    private double cancellationFee;

    public Booking(int roomId, int hotelId, int guestId, Date startDate, Date endDate, double price, double cancellationFee) {
        this.roomId = roomId;
        this.hotelId = hotelId;
        this.guestId = guestId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.cancellationFee = cancellationFee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    public int getGuestId() {
        return guestId;
    }

    public void setGuestId(int guestId) {
        this.guestId = guestId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getCancellationFee() {
        return cancellationFee;
    }

    public void setCancellationFee(double cancellationFee) {
        this.cancellationFee = cancellationFee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return id == booking.id && roomId == booking.roomId && hotelId == booking.hotelId && guestId == booking.guestId
                && Double.compare(price, booking.price) == 0
                && Double.compare(cancellationFee, booking.cancellationFee) == 0 &&
                Objects.equals(startDate, booking.startDate) && Objects.equals(endDate, booking.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomId, hotelId, guestId, startDate, endDate, price, cancellationFee);
    }
}
