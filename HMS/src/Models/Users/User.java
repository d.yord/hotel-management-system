package Models.Users;

import Models.Booking;
import Models.Enums.UserRole;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class User implements Serializable {
    public static int idCounter;
    private int id;
    private String username;
    private String password;
    private UserRole role;
    private List<Booking> bookings;

    public User(String username, String password, UserRole role, List<Booking> bookings) {
        this.id = User.idCounter++;
        this.username = username;
        this.password = password;
        this.role = role;
        this.bookings = bookings;
    }

    public static int getIdCounter() {
        return idCounter;
    }

    public static void setIdCounter(int idCounter) {
        User.idCounter = idCounter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && Objects.equals(username, user.username) && Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password);
    }

}
