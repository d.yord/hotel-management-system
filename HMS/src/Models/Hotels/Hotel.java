package Models.Hotels;

import Models.Enums.RoomType;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Hotel implements Serializable {
    private int id;
    private String name;
    private List<Room> rooms;
    private boolean pool;
    private boolean spa;
    private boolean restaurant;
    private boolean bar;
    private boolean parking;
    private boolean fitness;
    private Map<RoomType, Double> pricelist;
    private double cancellationFeePercentage;

    public Hotel(String name, List<Room> rooms, boolean pool, boolean spa, boolean restaurant, boolean bar,
                 boolean parking, boolean fitness, Map<RoomType, Double> pricelist, double cancellationFeePercentage) {
        this.name = name;
        this.rooms = rooms;
        this.pool = pool;
        this.spa = spa;
        this.restaurant = restaurant;
        this.bar = bar;
        this.parking = parking;
        this.fitness = fitness;
        this.pricelist = pricelist;
        this.cancellationFeePercentage = cancellationFeePercentage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public boolean isPool() {
        return pool;
    }

    public void setPool(boolean pool) {
        this.pool = pool;
    }

    public boolean isSpa() {
        return spa;
    }

    public void setSpa(boolean spa) {
        this.spa = spa;
    }

    public boolean isRestaurant() {
        return restaurant;
    }

    public void setRestaurant(boolean restaurant) {
        this.restaurant = restaurant;
    }

    public boolean isBar() {
        return bar;
    }

    public void setBar(boolean bar) {
        this.bar = bar;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    public boolean isFitness() {
        return fitness;
    }

    public void setFitness(boolean fitness) {
        this.fitness = fitness;
    }

    public Map<RoomType, Double> getPricelist() {
        return pricelist;
    }

    public void setPricelist(Map<RoomType, Double> pricelist) {
        this.pricelist = pricelist;
    }

    public double getCancellationFeePercentage() {
        return cancellationFeePercentage;
    }

    public void setCancellationFeePercentage(double cancellationFeePercentage) {
        this.cancellationFeePercentage = cancellationFeePercentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return id == hotel.id && Objects.equals(name, hotel.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "name='" + name + '\'' +
                ", pool=" + pool +
                ", spa=" + spa +
                ", restaurant=" + restaurant +
                ", bar=" + bar +
                ", parking=" + parking +
                ", fitness=" + fitness +
                ", price list=" + pricelist +
                ", cancellationFeePercentage=" + cancellationFeePercentage +
                '}';
    }
}
