package Models.Hotels;

import Models.Enums.BedType;
import Models.Enums.RoomType;

import java.util.List;
import java.util.Objects;

public class Room {
    private int id;
    private int number;
    private RoomType type;
    private List<BedType> beds;
    private int capacity;
    private boolean tv;
    private boolean kitchen;
    private boolean wifi;
    private boolean isSharedBathroom;

    public Room(int number, RoomType type, List<BedType> beds, int capacity, boolean tv, boolean kitchen,
                boolean wifi, boolean isSharedBathroom) {
        this.number = number;
        this.type = type;
        this.beds = beds;
        this.capacity = capacity;
        this.tv = tv;
        this.kitchen = kitchen;
        this.wifi = wifi;
        this.isSharedBathroom = isSharedBathroom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public RoomType getType() {
        return type;
    }

    public void setType(RoomType type) {
        this.type = type;
    }

    public List<BedType> getBeds() {
        return beds;
    }

    public void setBeds(List<BedType> beds) {
        this.beds = beds;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isTv() {
        return tv;
    }

    public void setTv(boolean tv) {
        this.tv = tv;
    }

    public boolean isKitchen() {
        return kitchen;
    }

    public void setKitchen(boolean kitchen) {
        this.kitchen = kitchen;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isSharedBathroom() {
        return isSharedBathroom;
    }

    public void setSharedBathroom(boolean sharedBathroom) {
        isSharedBathroom = sharedBathroom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return id == room.id && number == room.number && capacity == room.capacity && tv == room.tv
                && kitchen == room.kitchen && wifi == room.wifi && isSharedBathroom == room.isSharedBathroom
                && type == room.type && Objects.equals(beds, room.beds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, type, beds, capacity, tv, kitchen, wifi, isSharedBathroom);
    }
}
