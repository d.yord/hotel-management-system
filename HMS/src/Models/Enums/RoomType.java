package Models.Enums;

public enum RoomType{
    Single,
    Double,
    Suite,
    Shared
}
