package Models.Enums;

public enum UserRole {
    GUEST,
    ADMIN
}
