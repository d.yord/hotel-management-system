package Models.Enums;

public enum BedType {
    Single,
    Double,
    KingSize,
    QueenSize,
    Sofa,
    Portable

}
